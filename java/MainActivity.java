package com.example.tp3and;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {


   ListView List;
   Cursor Curseur;
   SimpleCursorAdapter CAdapter;
   WeatherDbHelper weatherDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        List = findViewById(R.id.ListeVille);
        weatherDbHelper = new WeatherDbHelper(this);
        weatherDbHelper.populate();
        CAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, weatherDbHelper.fetchAllCities () , new String[] { WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper. COLUMN_COUNTRY }, new int[] { android.R.id.text1, android.R.id.text2});
        List.setAdapter(CAdapter);




        // Button pour ajouter ville

        FloatingActionButton AddC = findViewById(R.id.AddC);
        AddC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), NewCityActivity.class);
                myIntent.putExtra("NewCitySelected", (Bundle) null);
                startActivity(myIntent);
            }
        });

        //  button pour acceder a la page cityactivity

        List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                Intent myIntent = new Intent(v.getContext(), CityActivity.class);
                Curseur = (Cursor) List.getItemAtPosition(position);
                City Select_Ville = WeatherDbHelper.cursorToCity(Curseur);
                Log.d("opening city activity", Select_Ville.toString());
                myIntent.putExtra("Select_Ville", Select_Ville);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
